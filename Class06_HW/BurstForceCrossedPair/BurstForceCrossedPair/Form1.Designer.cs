﻿namespace BurstForceCrossedPair
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_A = new System.Windows.Forms.Label();
            this.lbl_B = new System.Windows.Forms.Label();
            this.lbl_C = new System.Windows.Forms.Label();
            this.lbl_D = new System.Windows.Forms.Label();
            this.lbl_E = new System.Windows.Forms.Label();
            this.lbl_F = new System.Windows.Forms.Label();
            this.lbl_G = new System.Windows.Forms.Label();
            this.lbl_H = new System.Windows.Forms.Label();
            this.lbl_I = new System.Windows.Forms.Label();
            this.lbl_J = new System.Windows.Forms.Label();
            this.lbl_K = new System.Windows.Forms.Label();
            this.lbl_L = new System.Windows.Forms.Label();
            this.btn_Random = new System.Windows.Forms.Button();
            this.btn_ShowPair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_A
            // 
            this.lbl_A.AutoSize = true;
            this.lbl_A.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_A.Location = new System.Drawing.Point(0, 0);
            this.lbl_A.Name = "lbl_A";
            this.lbl_A.Size = new System.Drawing.Size(33, 31);
            this.lbl_A.TabIndex = 0;
            this.lbl_A.Tag = "Item";
            this.lbl_A.Text = "A";
            // 
            // lbl_B
            // 
            this.lbl_B.AutoSize = true;
            this.lbl_B.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_B.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_B.Location = new System.Drawing.Point(0, 0);
            this.lbl_B.Name = "lbl_B";
            this.lbl_B.Size = new System.Drawing.Size(33, 31);
            this.lbl_B.TabIndex = 1;
            this.lbl_B.Tag = "Item";
            this.lbl_B.Text = "B";
            // 
            // lbl_C
            // 
            this.lbl_C.AutoSize = true;
            this.lbl_C.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_C.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_C.Location = new System.Drawing.Point(0, 0);
            this.lbl_C.Name = "lbl_C";
            this.lbl_C.Size = new System.Drawing.Size(35, 31);
            this.lbl_C.TabIndex = 2;
            this.lbl_C.Tag = "Item";
            this.lbl_C.Text = "C";
            // 
            // lbl_D
            // 
            this.lbl_D.AutoSize = true;
            this.lbl_D.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_D.Location = new System.Drawing.Point(0, 0);
            this.lbl_D.Name = "lbl_D";
            this.lbl_D.Size = new System.Drawing.Size(35, 31);
            this.lbl_D.TabIndex = 3;
            this.lbl_D.Tag = "Item";
            this.lbl_D.Text = "D";
            // 
            // lbl_E
            // 
            this.lbl_E.AutoSize = true;
            this.lbl_E.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_E.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_E.Location = new System.Drawing.Point(0, 0);
            this.lbl_E.Name = "lbl_E";
            this.lbl_E.Size = new System.Drawing.Size(33, 31);
            this.lbl_E.TabIndex = 4;
            this.lbl_E.Tag = "Item";
            this.lbl_E.Text = "E";
            // 
            // lbl_F
            // 
            this.lbl_F.AutoSize = true;
            this.lbl_F.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_F.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_F.Location = new System.Drawing.Point(0, 0);
            this.lbl_F.Name = "lbl_F";
            this.lbl_F.Size = new System.Drawing.Size(32, 31);
            this.lbl_F.TabIndex = 5;
            this.lbl_F.Tag = "Item";
            this.lbl_F.Text = "F";
            // 
            // lbl_G
            // 
            this.lbl_G.AutoSize = true;
            this.lbl_G.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_G.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_G.Location = new System.Drawing.Point(0, 0);
            this.lbl_G.Name = "lbl_G";
            this.lbl_G.Size = new System.Drawing.Size(36, 31);
            this.lbl_G.TabIndex = 6;
            this.lbl_G.Tag = "Item";
            this.lbl_G.Text = "G";
            // 
            // lbl_H
            // 
            this.lbl_H.AutoSize = true;
            this.lbl_H.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_H.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_H.Location = new System.Drawing.Point(0, 0);
            this.lbl_H.Name = "lbl_H";
            this.lbl_H.Size = new System.Drawing.Size(35, 31);
            this.lbl_H.TabIndex = 7;
            this.lbl_H.Tag = "Item";
            this.lbl_H.Text = "H";
            // 
            // lbl_I
            // 
            this.lbl_I.AutoSize = true;
            this.lbl_I.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_I.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_I.Location = new System.Drawing.Point(0, 0);
            this.lbl_I.Name = "lbl_I";
            this.lbl_I.Size = new System.Drawing.Size(23, 31);
            this.lbl_I.TabIndex = 8;
            this.lbl_I.Tag = "Item";
            this.lbl_I.Text = "I";
            // 
            // lbl_J
            // 
            this.lbl_J.AutoSize = true;
            this.lbl_J.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_J.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_J.Location = new System.Drawing.Point(0, 0);
            this.lbl_J.Name = "lbl_J";
            this.lbl_J.Size = new System.Drawing.Size(29, 31);
            this.lbl_J.TabIndex = 9;
            this.lbl_J.Tag = "Item";
            this.lbl_J.Text = "J";
            // 
            // lbl_K
            // 
            this.lbl_K.AutoSize = true;
            this.lbl_K.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_K.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_K.Location = new System.Drawing.Point(0, 0);
            this.lbl_K.Name = "lbl_K";
            this.lbl_K.Size = new System.Drawing.Size(33, 31);
            this.lbl_K.TabIndex = 10;
            this.lbl_K.Tag = "Item";
            this.lbl_K.Text = "K";
            // 
            // lbl_L
            // 
            this.lbl_L.AutoSize = true;
            this.lbl_L.BackColor = System.Drawing.Color.AntiqueWhite;
            this.lbl_L.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_L.Location = new System.Drawing.Point(0, 0);
            this.lbl_L.Name = "lbl_L";
            this.lbl_L.Size = new System.Drawing.Size(30, 31);
            this.lbl_L.TabIndex = 11;
            this.lbl_L.Tag = "Item";
            this.lbl_L.Text = "L";
            // 
            // btn_Random
            // 
            this.btn_Random.Location = new System.Drawing.Point(12, 12);
            this.btn_Random.Name = "btn_Random";
            this.btn_Random.Size = new System.Drawing.Size(95, 23);
            this.btn_Random.TabIndex = 12;
            this.btn_Random.Text = "Random Object";
            this.btn_Random.UseVisualStyleBackColor = true;
            this.btn_Random.Click += new System.EventHandler(this.btn_Random_Click);
            // 
            // btn_ShowPair
            // 
            this.btn_ShowPair.Location = new System.Drawing.Point(113, 12);
            this.btn_ShowPair.Name = "btn_ShowPair";
            this.btn_ShowPair.Size = new System.Drawing.Size(47, 23);
            this.btn_ShowPair.TabIndex = 13;
            this.btn_ShowPair.Text = "Pair";
            this.btn_ShowPair.UseVisualStyleBackColor = true;
            this.btn_ShowPair.Click += new System.EventHandler(this.btn_ShowPair_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 562);
            this.Controls.Add(this.btn_ShowPair);
            this.Controls.Add(this.btn_Random);
            this.Controls.Add(this.lbl_L);
            this.Controls.Add(this.lbl_K);
            this.Controls.Add(this.lbl_J);
            this.Controls.Add(this.lbl_I);
            this.Controls.Add(this.lbl_H);
            this.Controls.Add(this.lbl_G);
            this.Controls.Add(this.lbl_F);
            this.Controls.Add(this.lbl_E);
            this.Controls.Add(this.lbl_D);
            this.Controls.Add(this.lbl_C);
            this.Controls.Add(this.lbl_B);
            this.Controls.Add(this.lbl_A);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_A;
        private System.Windows.Forms.Label lbl_B;
        private System.Windows.Forms.Label lbl_C;
        private System.Windows.Forms.Label lbl_D;
        private System.Windows.Forms.Label lbl_E;
        private System.Windows.Forms.Label lbl_F;
        private System.Windows.Forms.Label lbl_G;
        private System.Windows.Forms.Label lbl_H;
        private System.Windows.Forms.Label lbl_I;
        private System.Windows.Forms.Label lbl_J;
        private System.Windows.Forms.Label lbl_K;
        private System.Windows.Forms.Label lbl_L;
        private System.Windows.Forms.Button btn_Random;
        private System.Windows.Forms.Button btn_ShowPair;
    }
}

