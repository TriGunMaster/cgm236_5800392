﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BurstForceCrossedPair
{
    public partial class Form1 : Form
    {
        List<Label> objs;
        Label A,B;
        int distance;
        public Form1()
        {
            InitializeComponent();
            objs = new List<Label>();
            foreach (Control data in this.Controls)
            {
                if (data.GetType() == typeof(Label))
                    objs.Add((Label)data);
            }
        }

        private void btn_Random_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            distance = 99999;
            foreach (Label data in objs)
            {
                data.Left = rnd.Next(50,500);
                data.Top = rnd.Next(50, 500);
                data.BackColor = Color.AntiqueWhite;
            }
        }

        private void btn_ShowPair_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < objs.Count; i++)
            {
                for (int z = i+1; z < objs.Count; z++)
                {
                    int cal_x = Math.Abs(objs[i].Location.X - objs[z].Location.X);
                    int cal_y = Math.Abs(objs[i].Location.Y - objs[z].Location.Y);

                    if (Math.Abs(cal_x + cal_y) < distance)
                    {
                        A = objs[i];
                        B = objs[z];
                        distance = Math.Abs(cal_x + cal_y);
                    }
                }
            }

            A.BackColor = Color.Red;
            B.BackColor = Color.Blue;
        }
    }
}
