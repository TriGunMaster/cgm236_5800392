﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            InsertionSorting();   
        }

        public static void InsertionSorting()
        {
            Arr nums = new Arr(10);

            Random rnd = new Random(100);

            for (int i = 0; i < 10; i++)
            {
                int rnum = (int)(rnd.Next(0, 100));
                //Console.Write(rnum);
                nums.Insert(rnum);
            }

            Console.WriteLine("Before sorting : ");
            nums.DisplayElements();

            nums.InsertionSort();

            Console.WriteLine("");
            Console.WriteLine("After sorting : ");
            nums.DisplayElements();

            Console.Read();
        }

        public static void SelectionSorting()
        {
            Arr nums = new Arr(10);

            Random rnd = new Random(100);

            for (int i = 0; i < 10; i++)
            {
                int rnum = (int)(rnd.Next(0, 100));
                //Console.Write(rnum);
                nums.Insert(rnum);
            }

            Console.WriteLine("Before sorting : ");
            nums.DisplayElements();

            nums.SelectionSort();

            Console.WriteLine("");
            Console.WriteLine("After sorting : ");
            nums.DisplayElements();

            Console.Read();
        }

        public static void BubbleSorting()
        {
            Arr nums = new Arr(10);

            Random rnd = new Random(100);

            for (int i = 0; i < 10; i++)
            {
                int rnum = (int)(rnd.Next(0, 100));
                //Console.Write(rnum);
                nums.Insert(rnum);
            }

            Console.WriteLine("Before sorting : ");
            nums.DisplayElements();

            Console.WriteLine("");
            Console.WriteLine("During sorting : ");
            nums.BubbleSort();

            Console.WriteLine("");
            Console.WriteLine("After sorting : ");
            nums.DisplayElements();

            Console.Read();
        }

        public static void Basic()
        {
            Arr nums = new Arr(10);

            Random rnd = new Random(100);

            for (int i = 0; i < 10; i++)
            {
                int rnum = (int)(rnd.Next(0, 100));
                //Console.Write(rnum);
                nums.Insert(rnum);
            }

            nums.DisplayElements();
            Console.Read();
        }
    }
}
