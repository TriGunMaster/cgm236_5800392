﻿using System;

namespace Sorting
{
    public class Arr
    {
        int[] arr;
        int upper;
        int numElements;

        public Arr(int size)
        {
            arr = new int[size];
            upper = size - 1;
            numElements = 0;
        }

        public void Insert(int value)
        {
            arr[numElements] = value;
            numElements++;
        }

        public void DisplayElements()
        {
            for (int i = 0; i <= upper; i++)
                Console.Write(arr[i] + " ");
            Console.WriteLine("");
        }

        public void Clear()
        {
            for (int i = 0; i <= upper; i++)
                arr[i] = 0;
            numElements = 0;
        }
        public void BubbleSort()
        {
            int temp, outer, inner = 0;

            for(outer = upper; outer >= -1;outer--)
            {
                for (inner = 0; inner <= outer - 1; inner++)
                {
                    if (arr[inner] > arr[inner + 1])
                    {
                        temp = arr[inner];
                        arr[inner] = arr[inner + 1];
                        arr[inner + 1] = temp;
                    }
                }
                this.DisplayElements();
            }
        }

        public void SelectionSort()
        {
            int min, temp, outer, inner = 0;

            for (outer = 0; outer <= upper; outer++)
            {
                min = outer;
                for (inner = outer + 1; inner <= upper; inner++)
                {
                    if (arr[inner] < arr[min])
                    {
                        min = inner;
                    }

                    temp = arr[outer];
                    arr[outer] = arr[min];
                    arr[min] = temp;
                }
            }
        }

        public void InsertionSort()
        {
            int temp, outer, inner = 0;

            for (outer = 1; outer <= upper; outer++)
            {
                temp = arr[outer];
                inner = outer;

                while (inner > 0 && arr[inner - 1] >= temp)
                {
                    arr[inner] = arr[inner - 1];
                    inner -= 1;
                }
                arr[inner] = temp;
            }
        }
    }
}