﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euclids_PseudoCode
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Insert Num1: ");
                int num1 = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Insert Num2: ");
                int num2 = Int32.Parse(Console.ReadLine());

                Console.WriteLine("Euc Value: " + func_Euclids(num1, num2));

                Console.ReadLine();
            }
        }

        static int func_Euclids(int m, int n)
        {
            int r = 0;
            while(n != 0)
            {
                r = m % n;
                m = n;
                n = r;
            }
            return m;
        }
    }
}
