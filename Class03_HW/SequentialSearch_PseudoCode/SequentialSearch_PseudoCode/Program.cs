﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SequentialSearch_PseudoCode
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] A = new int[5] { 10, 30, 50, 70, 90 };

            while (true)
                Console.WriteLine("Index:" + func_Search(A, Int32.Parse(Console.ReadLine())));
        }

        static int func_Search(int[] A,int K)
        {
            int i = 0;
            int n = A.Length;
            //int K = Int32.Parse(Console.ReadLine());
            //int[] A = new int[5] { 10, 30, 50, 70, 90 };

            while (i < n && A[i] != K)
                i = i + 1;

            if (i < n)
                return i;
            else
                return -1;
        }
    }
}
