﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptPlayer_Direction : MonoBehaviour {

    public string DirPositionName;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Wall"))
        {
            if (DirPositionName == "Up")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallUp = true;
                //Debug.Log("UP");
            }
            if (DirPositionName == "Down")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallDown = true;
                //Debug.Log("DOWN");
            }
            if (DirPositionName == "Left")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallLeft = true;
                //Debug.Log("LEFT");
            }
            if (DirPositionName == "Right")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallRight = true;
                //Debug.Log("RIGHT");
            }
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Wall"))
        {
            if (DirPositionName == "Up")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallUp = true;
                //Debug.Log("UP");
            }
            if (DirPositionName == "Down")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallDown = true;
                //Debug.Log("DOWN");
            }
            if (DirPositionName == "Left")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallLeft = true;
                //Debug.Log("LEFT");
            }
            if (DirPositionName == "Right")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallRight = true;
                //Debug.Log("RIGHT");
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Wall"))
        {
            if (DirPositionName == "Up")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallUp = false;
                //Debug.Log("UP");
            }
            if (DirPositionName == "Down")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallDown = false;
                //Debug.Log("DOWN");
            }
            if (DirPositionName == "Left")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallLeft = false;
                //Debug.Log("LEFT");
            }
            if (DirPositionName == "Right")
            {
                transform.parent.GetComponent<ScriptPlayerController>().m_WallRight = false;
                //Debug.Log("RIGHT");
            }
        }
    }
}
