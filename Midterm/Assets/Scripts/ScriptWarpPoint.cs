﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptWarpPoint : MonoBehaviour {

    public Transform WarpTo;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player") || col.CompareTag("Enemy"))
            col.transform.position = WarpTo.transform.position;
    }
}
