﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

public class ScriptGameManager : MonoBehaviour {

    public static bool isPlayerDead;
    public static bool isPlayerWin;
    public static bool isPlayerPause;
    public static bool isPlayerRespawn;
    public bool isSpawnItem;
    public Text lbl_score;
    public Text lbl_life;
    public Text lbl_round;
    public GameObject lbl_youwin, lbl_gameover, lbl_ready;
    public Transform RespawnOfPlayerPoint;
    public Sprite Idle_Sprite;
    public static int RoundCount;
    public static int RoundCount_CalSpeed;

    // wait for fix many hit enemy after status spirit
    // wait for fix enemy status chaos bug walk throught the wall
    void Start () {
        StartCoroutine(CheckPlayerSpawn());
        if (RoundCount == 0)
        {
            RoundCount = 1;
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (ScriptGameData.m_Score != int.Parse(lbl_score.text))
        {
            lbl_score.text = ScriptGameData.m_Score.ToString();
        }
        if (ScriptGameData.m_Life != int.Parse(lbl_life.text))
        {
            lbl_life.text = ScriptGameData.m_Life.ToString();
        }
        if (RoundCount != int.Parse(lbl_round.text))
        {
            lbl_round.text = RoundCount.ToString();
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (Time.timeScale == 0)
                Time.timeScale = 1;
            else
                Time.timeScale = 0;
        }

        if (Time.time - ScriptGameData.PlayerAliveTime >= 10 && !isSpawnItem)
        {
            isSpawnItem = true;
            switch (RoundCount)
            {
                case 0:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank8"));
                    break;
                case 1:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank7"));
                    break;
                case 2:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank6"));
                    break;
                case 3:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank5"));
                    break;
                case 4:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank4"));
                    break;
                case 5:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank3"));
                    break;
                case 6:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank2"));
                    break;
                default:
                    Instantiate(Resources.Load<GameObject>("Prefabs/Item_Bonus_Rank1"));
                    break;
            }
            //Debug.Log(RoundCount);
        }
	}

    void FixedUpdate()
    {
        if (GameObject.FindGameObjectsWithTag("Items").Length == 0 && !isPlayerWin)
        {
            isPlayerPause = true;
            isPlayerWin = true;
            StartCoroutine(CheckPlayerWin());
        }
        if (isPlayerDead)
        {
            StartCoroutine(CheckPlayerSpawnByDead());
            isPlayerDead = false;
        }
    }

    IEnumerator CheckPlayerSpawn()
    {
        ScriptGameData.m_StackEat = 0;
        isPlayerPause = true;
        // Setting Game
        // Set Enemy Spawn
        //foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
        //{
        //    Destroy(obj);
        //}

        //Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Red"));
        //Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Blue"));
        //Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Pink"));
        //Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Orange"));
        ScriptGameData.PlayerAliveTime = Time.time;
        GameObject.FindGameObjectWithTag("Player").transform.position = RespawnOfPlayerPoint.position;
        GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().Ani.speed = 0;
        yield return new WaitForSeconds(1f);
        int i = 0;
        while (true)
        {
            lbl_ready.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            lbl_ready.SetActive(false);
            yield return new WaitForSeconds(0.2f);
            if (i >= 6)
            {
                break;
            }
            i++;
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().Ani.speed = 1;
        GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().m_Horizontal = -1f;
        GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().m_Vertical = 0f;
        isPlayerPause = false;
        isPlayerWin = false;

    }

    IEnumerator CheckPlayerSpawnByDead()
    {
        isPlayerPause = true;
        yield return new WaitForSeconds(4f);

        if (ScriptGameData.m_Life <= 0)
        {
            int i = 0;
            while (true)
            {
                lbl_gameover.SetActive(true);
                yield return new WaitForSeconds(0.2f);
                lbl_gameover.SetActive(false);
                yield return new WaitForSeconds(0.2f);
                if (i >= 10)
                {
                    break;
                }
                i++;
            }

            isPlayerPause = false;

            // Load Menu Scene
            Application.LoadLevel("Menu");
        }
        else
        {
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                Destroy(obj);
            }

            Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Red"));
            Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Blue"));
            Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Pink"));
            Instantiate(Resources.Load<GameObject>("Prefabs/Enemy_Orange"));

            // Setting Game
            // Set Enemy Spawn
            isPlayerRespawn = true;
            ScriptGameData.PlayerAliveTime = Time.time;
            GameObject.FindGameObjectWithTag("Player").transform.position = RespawnOfPlayerPoint.position;
            GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().Ani.transform.localRotation = Quaternion.Euler(0, 0, 90f);
            GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().Ani.SetTrigger("Up");
            GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().Ani.speed = 0;

            yield return new WaitForSeconds(0.1f);
            isPlayerRespawn = false;

            ScriptGameData.m_Life--;
            int i = 0;
            while (true)
            {
                lbl_ready.SetActive(true);
                yield return new WaitForSeconds(0.2f);
                lbl_ready.SetActive(false);
                yield return new WaitForSeconds(0.2f);
                if (i >= 6)
                {
                    break;
                }
                i++;
            }
            GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().Ani.speed = 1;
            GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().m_Horizontal = -1f;
            GameObject.FindGameObjectWithTag("Player").GetComponent<ScriptPlayerController>().m_Vertical = 0f;
            isPlayerPause = false;
        }
    }

    IEnumerator CheckPlayerWin()
    {
        yield return new WaitForSeconds(2f);
        //lbl_youwin.SetActive(true);

        int i = 0;
        while(true)
        {
            i++;
            yield return new WaitForSeconds(1f);
            if (i >= 1)
            {
                break;
            }
        }

        if(RoundCount_CalSpeed != RoundCount && RoundCount <= 10)
        {
            RoundCount_CalSpeed = RoundCount;
        }
        else
        {
            RoundCount_CalSpeed = 10;
        }
        RoundCount++;
        Application.LoadLevel(Application.loadedLevel);
    }
}
