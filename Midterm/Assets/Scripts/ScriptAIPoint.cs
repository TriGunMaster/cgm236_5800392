﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptAIPoint : MonoBehaviour {

    public bool m_WallUp, m_WallDown, m_WallLeft, m_WallRight;
    public float WayAvailable;
    public float m_WallUp_Percent, m_WallDown_Percent, m_WallLeft_Percent, m_WallRight_Percent;
    public float Percent_Stack;
    public bool isStart;
    public string DeadWay, PlayerWay;
	void Start () {
        GetComponent<SpriteRenderer>().enabled = false;
        Percent_Stack = 0;
        WayAvailable = 4;
        isStart = true;
        //if (m_WallUp)
        //    WayAvailable++;
        //if (m_WallDown)
        //    WayAvailable++;
        //if (m_WallLeft)
        //    WayAvailable++;
        //if (m_WallRight)
        //    WayAvailable++;

        //float ChangePerWay = 100 / WayAvailable;

        //m_WallUp_Percent = m_WallDown_Percent = m_WallLeft_Percent = m_WallRight_Percent = ChangePerWay;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    string RandomWay()
    {
        string direction = "";

        float ChangeToPlayer = ScriptGameData.PlayerAliveTime;

        //if ((Time.time - ChangeToPlayer) >= Random.Range(0, 100))
        if (WayAvailable == 100)
        {
            // Lock to player
            Debug.Log("Lock Player");
        }
        else
        {
            // Normal Random
            float RandomWay = Random.Range(0, 100);

            if (isStart)
            {
                if (!m_WallUp)
                {
                    //Debug.Log(Percent_Stack);
                    Percent_Stack += 100 / WayAvailable;
                    m_WallUp_Percent = Percent_Stack;
                }
                if (!m_WallDown)
                {
                    //Debug.Log(Percent_Stack);
                    Percent_Stack += 100 / WayAvailable;
                    m_WallDown_Percent = Percent_Stack;
                }
                if (!m_WallLeft)
                {
                    //Debug.Log(Percent_Stack);
                    Percent_Stack += 100 / WayAvailable;
                    m_WallLeft_Percent = Percent_Stack;
                }
                if (!m_WallRight)
                {
                    //Debug.Log(Percent_Stack);
                    Percent_Stack += 100 / WayAvailable;
                    m_WallRight_Percent = Percent_Stack;
                }
                isStart = false;
            }

            if (RandomWay < m_WallUp_Percent && !m_WallUp)
                direction = "Up";
            else if (RandomWay < m_WallDown_Percent && !m_WallDown)
                direction = "Down";
            else if (RandomWay < m_WallLeft_Percent && !m_WallLeft)
                direction = "Left";
            else if (RandomWay < m_WallRight_Percent && !m_WallRight)
                direction = "Right";

            //Debug.Log("Normal Way" + RandomWay + "|" + direction + "|" + m_WallUp_Percent + "|" + m_WallDown_Percent + "|" + m_WallLeft_Percent + "|" + m_WallRight_Percent);
        }

        // Random TargetPlayer Change
        return direction;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            if (col.GetComponent<ScriptEnemyController>().isDead)
            {
                col.GetComponent<ScriptEnemyController>().isTurn = true;
                col.GetComponent<ScriptEnemyController>().AIPoint = transform.position;
                //Random
                col.GetComponent<ScriptEnemyController>().WalkDirection = DeadWay;
                //DeadWay = null;
                //Debug.Log("EnemyDead");
            }
            else if (col.GetComponent<ScriptEnemyController>().isLockPlayer)
            {
                col.GetComponent<ScriptEnemyController>().isTurn = true;
                col.GetComponent<ScriptEnemyController>().AIPoint = transform.position;
                //Random
                col.GetComponent<ScriptEnemyController>().WalkDirection = PlayerWay;
                //PlayerWay = null;
                //Debug.Log("EnemyDead");
            }
            else
            {
                col.GetComponent<ScriptEnemyController>().isTurn = true;
                col.GetComponent<ScriptEnemyController>().AIPoint = transform.position;
                //Random
                col.GetComponent<ScriptEnemyController>().WalkDirection = RandomWay();
            }
            //Debug.Log("Enter");
        }
    }
}
