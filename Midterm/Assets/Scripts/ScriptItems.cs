﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptItems : MonoBehaviour {

    public string ItemType;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("PlayerBodySize"))
        {
            if (ItemType == "Small")
            {
                ScriptGameData.m_Score += 10;
                Destroy(gameObject);
            }
            else if (ItemType == "Big")
            {
                ScriptGameData.m_Score += 50;
                ScriptGameData.m_StackEat = 0;
                foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
                {
                    obj.GetComponent<ScriptEnemyController>().isStartChaos = true;
                }
                Destroy(gameObject);
            }
            else if (ItemType == "Rank8")
            {
                ScriptGameData.m_Score += 100;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore100"));
                Destroy(gameObject);
            }
            else if (ItemType == "Rank7")
            {
                ScriptGameData.m_Score += 100;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore100"));
                Destroy(gameObject);
            }
            else if (ItemType == "Rank6")
            {
                ScriptGameData.m_Score += 300;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore300"));
                Destroy(gameObject);
            }
            else if (ItemType == "Rank5")
            {
                ScriptGameData.m_Score += 500;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore500"));
                Destroy(gameObject);
            }
            else if (ItemType == "Rank4")
            {
                ScriptGameData.m_Score += 700;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore700"));
                Destroy(gameObject);
            }
            else if (ItemType == "Rank3")
            {
                ScriptGameData.m_Score += 2000;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore2000"));
                Destroy(gameObject);
            }
            else if (ItemType == "Rank2")
            {
                ScriptGameData.m_Score += 3000;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore3000"));
                Destroy(gameObject);
            }
            else if (ItemType == "Rank1")
            {
                ScriptGameData.m_Score += 5000;
                Instantiate(Resources.Load<GameObject>("Prefabs/FoodScore5000"));
                Destroy(gameObject);
            }
        }
    }
}
