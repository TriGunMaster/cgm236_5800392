﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptAIPoint_Direction : MonoBehaviour {

    public string WayPositionName;
	void Start () {
        GetComponent<SpriteRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Enter:" + col.name + "|" + WayPositionName);
        if (col.CompareTag("Enemy"))
        {
            if (col.GetComponent<ScriptEnemyController>().isRespawn)
                return;
            string dir_player = "";
            string dir_enemydead = "";
            if (col.GetComponent<ScriptEnemyController>().isDead && !col.GetComponent<ScriptEnemyController>().isBase)
            {
                float cal_posx = Mathf.Abs(transform.position.x - col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.x);
                float cal_posy = Mathf.Abs(transform.position.y - col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.y);
                if (cal_posx > cal_posy)
                {
                    if (transform.position.x < col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                    {
                        // Left
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Right";
                        dir_enemydead = "DXXRight";
                        //Debug.Log("XXDead Right:");

                    }
                    else if (transform.position.x > col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                    {
                        //Right
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Left";
                        dir_enemydead = "DXXLeft";
                        //Debug.Log("XXDead Left:");
                    }
                    else if (transform.position.y < col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                    {
                        // Down
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Up";
                        dir_enemydead = "DXXUp";
                        //Debug.Log("XXDead Up:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.y > col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                    {
                        //Up
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Down";
                        dir_enemydead = "DXXDown";
                        //Debug.Log("XDead Down:" + cal_posx + "|" + cal_posy + "|");
                    }
                }
                else
                {
                    if (transform.position.y < col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                    {
                        // Down
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Up";
                        dir_enemydead = "DYYUp";
                        //Debug.Log("YYDead Up:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.y > col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                    {
                        //Up
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Down";
                        dir_enemydead = "DYYDown";
                        //Debug.Log("YYDead Down:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.x < col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                    {
                        // Left
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Left";
                        dir_enemydead = "DYYLeft";
                        //Debug.Log("YYDead Right:" + cal_posx + "|" + cal_posy + "|");

                    }
                    else if (transform.position.x > col.GetComponent<ScriptEnemyController>().BaseOfEnemy.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                    {
                        //Right
                        transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Right";
                        dir_enemydead = "DYYRight";
                        //Debug.Log("YYDead Left:" + cal_posx + "|" + cal_posy + "|");
                    }
                }
            }

            if (col.GetComponent<ScriptEnemyController>().isLockPlayer && !col.GetComponent<ScriptEnemyController>().isDead && !col.GetComponent<ScriptEnemyController>().isChaos)
            {
                float cal_posx = Mathf.Abs(transform.position.x - GameObject.FindGameObjectWithTag("Player").transform.position.x);
                float cal_posy = Mathf.Abs(transform.position.y - GameObject.FindGameObjectWithTag("Player").transform.position.y);
                //string dir = "";

                if (cal_posx > cal_posy)
                {
                    if (transform.position.x < GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                    {
                        // Left
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Right";
                        dir_player = "XXRight";
                        //Debug.Log("XXDead Right:");
                    }
                    else if (transform.position.x > GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                    {
                        //Right
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Left";
                        dir_player = "XXLeft";
                        //Debug.Log("XXDead Left:");
                    }
                    else if (transform.position.y < GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                    {
                        // Down
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Up";
                        dir_player = "XXUp";
                        //Debug.Log("XXDead Up:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.y > GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                    {
                        //Up
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Down";
                        dir_player = "XXDown";
                        //Debug.Log("XDead Down:" + cal_posx + "|" + cal_posy + "|");
                    }
                }
                else
                {
                    if (transform.position.y < GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                    {
                        // Down
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Up";
                        dir_player = "YYUp";
                        //Debug.Log("YYDead Up:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.y > GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                    {
                        //Up
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Down";
                        dir_player = "YYDown";
                        //Debug.Log("YYDead Down:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.x < GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                    {
                        // Left
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Left";
                        dir_player = "YYLeft";
                        //Debug.Log("YYDead Right:" + cal_posx + "|" + cal_posy + "|");

                    }
                    else if (transform.position.x > GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                    {
                        //Right
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Right";
                        dir_player = "YYRight";
                        //Debug.Log("YYDead Left:" + cal_posx + "|" + cal_posy + "|");
                    }
                }

                //Debug.Log("Fail Rnd:" + dir + "|" + transform.parent.GetComponent<ScriptAIPoint>().m_WallUp + "|" + transform.parent.GetComponent<ScriptAIPoint>().m_WallDown + "|" + transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft + "|" + transform.parent.GetComponent<ScriptAIPoint>().m_WallRight);
            }

            if (col.GetComponent<ScriptEnemyController>().isChaos && !col.GetComponent<ScriptEnemyController>().isDead)
            {
                float cal_posx = Mathf.Abs(transform.position.x - GameObject.FindGameObjectWithTag("Player").transform.position.x);
                float cal_posy = Mathf.Abs(transform.position.y - GameObject.FindGameObjectWithTag("Player").transform.position.y);
                if (cal_posx > cal_posy)
                {
                    if (transform.position.x > GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                    {
                        // Left
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Right";
                        dir_player = "CXXRight";
                        //Debug.Log("XXDead Right:");

                    }
                    else if (transform.position.x < GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                    {
                        //Right
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Left";
                        dir_player = "CXXLeft";
                        //Debug.Log("XXDead Left:");
                    }
                    else if (transform.position.y > GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                    {
                        // Down
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Up";
                        dir_player = "CXXUp";
                        //Debug.Log("XXDead Up:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.y < GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                    {
                        //Up
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Down";
                        dir_player = "CXXDown";
                        //Debug.Log("XDead Down:" + cal_posx + "|" + cal_posy + "|");
                    }
                }
                else
                {
                    if (transform.position.y > GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                    {
                        // Down
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Up";
                        dir_player = "CYYUp";
                        //Debug.Log("YYDead Up:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.y < GameObject.FindGameObjectWithTag("Player").transform.position.y && !transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                    {
                        //Up
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Down";
                        dir_player = "CYYDown";
                        //Debug.Log("YYDead Down:" + cal_posx + "|" + cal_posy + "|");
                    }
                    else if (transform.position.x > GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                    {
                        // Left
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Left";
                        dir_player = "CYYLeft";
                        //Debug.Log("YYDead Right:" + cal_posx + "|" + cal_posy + "|");

                    }
                    else if (transform.position.x < GameObject.FindGameObjectWithTag("Player").transform.position.x && !transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                    {
                        //Right
                        transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Right";
                        dir_player = "CYYRight";
                        //Debug.Log("YYDead Left:" + cal_posx + "|" + cal_posy + "|");
                    }
                }
            }

            col.GetComponent<ScriptEnemyController>().LastWalkDirection = WayPositionName;
            if (WayPositionName == "Up")
            {
                col.GetComponent<ScriptEnemyController>().m_WallUp = true;
                col.GetComponent<ScriptEnemyController>().m_WallDown = transform.parent.GetComponent<ScriptAIPoint>().m_WallDown;
                col.GetComponent<ScriptEnemyController>().m_WallLeft = transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft;
                col.GetComponent<ScriptEnemyController>().m_WallRight = transform.parent.GetComponent<ScriptAIPoint>().m_WallRight;
            }
            if (WayPositionName == "Down")
            {
                col.GetComponent<ScriptEnemyController>().m_WallUp = transform.parent.GetComponent<ScriptAIPoint>().m_WallUp;
                col.GetComponent<ScriptEnemyController>().m_WallDown = true;
                col.GetComponent<ScriptEnemyController>().m_WallLeft = transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft;
                col.GetComponent<ScriptEnemyController>().m_WallRight = transform.parent.GetComponent<ScriptAIPoint>().m_WallRight;
            }
            if (WayPositionName == "Left")
            {
                col.GetComponent<ScriptEnemyController>().m_WallUp = transform.parent.GetComponent<ScriptAIPoint>().m_WallUp;
                col.GetComponent<ScriptEnemyController>().m_WallDown = transform.parent.GetComponent<ScriptAIPoint>().m_WallDown;
                col.GetComponent<ScriptEnemyController>().m_WallLeft = true;
                col.GetComponent<ScriptEnemyController>().m_WallRight = transform.parent.GetComponent<ScriptAIPoint>().m_WallRight;
            }
            if (WayPositionName == "Right")
            {
                col.GetComponent<ScriptEnemyController>().m_WallUp = transform.parent.GetComponent<ScriptAIPoint>().m_WallUp;
                col.GetComponent<ScriptEnemyController>().m_WallDown = transform.parent.GetComponent<ScriptAIPoint>().m_WallDown;
                col.GetComponent<ScriptEnemyController>().m_WallLeft = transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft;
                col.GetComponent<ScriptEnemyController>().m_WallRight = true;
            }

            if (dir_enemydead == null || dir_enemydead == "" && col.GetComponent<ScriptEnemyController>().isDead && !col.GetComponent<ScriptEnemyController>().isBase) // Fixbug Random on Hit Wall
            {
                if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Up";
                }
                else if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Down";
                }
                else if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Left";
                }
                else if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().DeadWay = "Right";
                }
                //Debug.Log("Rnd Again:" + col.transform.name + "|" + transform.parent.GetComponent<ScriptAIPoint>().PlayerWay);
            }

            if (dir_player == null || dir_player == "" && !col.GetComponent<ScriptEnemyController>().isDead) // Fixbug Random on Hit Wall
            {
                if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallUp)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Up";
                }
                else if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallDown)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Down";
                }
                else if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Left";
                }
                else if (!transform.parent.GetComponent<ScriptAIPoint>().m_WallRight)
                {
                    transform.parent.GetComponent<ScriptAIPoint>().PlayerWay = "Right";
                }
                //Debug.Log("Rnd Again:" + col.transform.name + "|" + transform.parent.GetComponent<ScriptAIPoint>().PlayerWay);
            }
        }

        if (col.CompareTag("Wall"))
        {
            if (WayPositionName == "Up")
            {
                transform.parent.GetComponent<ScriptAIPoint>().m_WallUp = true;
                transform.parent.GetComponent<ScriptAIPoint>().WayAvailable--;
                //Debug.Log("UP");
            }
            if (WayPositionName == "Down")
            {
                transform.parent.GetComponent<ScriptAIPoint>().m_WallDown = true;
                transform.parent.GetComponent<ScriptAIPoint>().WayAvailable--;
                //Debug.Log("DOWN");
            }
            if (WayPositionName == "Left")
            {
                transform.parent.GetComponent<ScriptAIPoint>().m_WallLeft = true;
                transform.parent.GetComponent<ScriptAIPoint>().WayAvailable--;
                //Debug.Log("LEFT");
            }
            if (WayPositionName == "Right")
            {
                transform.parent.GetComponent<ScriptAIPoint>().m_WallRight = true;
                transform.parent.GetComponent<ScriptAIPoint>().WayAvailable--;
                //Debug.Log("RIGHT");
            }
        }
    }

    //void OnTriggerStay2D(Collider2D col)
    //{
    //    Debug.Log("Stay:" + col.name + "|" + WayPositionName);
    //}
}
