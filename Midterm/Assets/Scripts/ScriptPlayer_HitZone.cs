﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptPlayer_HitZone : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            if (col.GetComponent<ScriptEnemyController>().isChaos && !col.GetComponent<ScriptEnemyController>().isDead)
            {
                ScriptGameData.m_StackEat++;
                col.GetComponent<ScriptEnemyController>().isPlayerEat = true;
                switch (ScriptGameData.m_StackEat)
                {
                    case 1:
                        Instantiate(Resources.Load<GameObject>("Prefabs/KillScore200"), col.transform.position,Quaternion.identity);
                        break;
                    case 2:
                        Instantiate(Resources.Load<GameObject>("Prefabs/KillScore400"), col.transform.position,Quaternion.identity);
                        break;
                    case 3:
                        Instantiate(Resources.Load<GameObject>("Prefabs/KillScore900"), col.transform.position,Quaternion.identity);
                        break;
                    case 4:
                        Instantiate(Resources.Load<GameObject>("Prefabs/KillScore1600"), col.transform.position,Quaternion.identity);
                        ScriptGameData.m_StackEat = 0;
                        break;
                }
            }
            else if (!col.GetComponent<ScriptEnemyController>().isDead)
            {
                ScriptGameManager.isPlayerDead = true;
                transform.parent.GetComponent<ScriptPlayerController>().Ani.speed = 1;
                transform.parent.GetComponent<ScriptPlayerController>().Ani.SetTrigger("Dead");
                transform.parent.GetComponent<ScriptPlayerController>().Ani.transform.rotation = Quaternion.Euler(0, 0, 0f);
            }
        }
    }
}
