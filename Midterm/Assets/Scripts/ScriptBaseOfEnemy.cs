﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBaseOfEnemy : MonoBehaviour {

    public Transform AIPointLeft, AIPointRight, RespawnOfEnemyPoint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            //col.GetComponent<ScriptEnemyController>().isDead = false;
            if (col.GetComponent<ScriptEnemyController>().isDead)
            {
                col.GetComponent<ScriptEnemyController>().isTurn = true;
                col.GetComponent<ScriptEnemyController>().isBase = true;
                col.GetComponent<ScriptEnemyController>().isChaos = false;
                col.GetComponent<ScriptEnemyController>().AIPoint = new Vector2(RespawnOfEnemyPoint.position.x, RespawnOfEnemyPoint.position.y);
                col.GetComponent<ScriptEnemyController>().WalkDirection = "Down";
                col.GetComponent<ScriptEnemyController>().m_Vertical = -1;
                col.GetComponent<ScriptEnemyController>().m_Horizontal = 0f;
                col.transform.position = transform.position;
            }
            else if(col.GetComponent<ScriptEnemyController>().isRespawn)
            {
                float rand = Random.Range(0, 100);
                col.GetComponent<ScriptEnemyController>().isRespawn = false;
                if(rand < 50)
                {
                    col.GetComponent<ScriptEnemyController>().AIPoint = AIPointLeft.position;
                    col.GetComponent<ScriptEnemyController>().WalkDirection = "Left";
                    //col.GetComponent<ScriptEnemyController>().isTurn = true;
                    col.GetComponent<ScriptEnemyController>().m_Vertical = 0;
                    col.GetComponent<ScriptEnemyController>().m_Horizontal = -1f;
                }
                else
                {
                    col.GetComponent<ScriptEnemyController>().AIPoint = AIPointRight.position;
                    col.GetComponent<ScriptEnemyController>().WalkDirection = "Right";
                    //col.GetComponent<ScriptEnemyController>().isTurn = true;
                    col.GetComponent<ScriptEnemyController>().m_Vertical = 0;
                    col.GetComponent<ScriptEnemyController>().m_Horizontal = 1f;
                }
                col.GetComponent<ScriptEnemyController>().isNotIdle = true;
                //col.GetComponent<ScriptEnemyController>().m_MoveSpeed = 2.5f;
                col.transform.position = transform.position;
                //Debug.Log("Respawn dir:" + col.GetComponent<ScriptEnemyController>().WalkDirection + "| Rnd:" + rand);
            }
        }
    }
}
