﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ScriptMenu : MonoBehaviour {

    public Button btn_StartGame;
    public Button btn_Exit;
    // Use this for initialization
    void Start () {
        if (SceneManager.GetActiveScene().name != "GamePlay")
        btn_StartGame.OnSelect(null);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void btn_StartGame_Click()
    {
        Application.LoadLevel("GamePlay");
    }
    public void btn_Exit_Click()
    {
        Application.Quit();
    }
}
