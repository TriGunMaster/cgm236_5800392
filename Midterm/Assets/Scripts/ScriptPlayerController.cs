﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptPlayerController : MonoBehaviour {

    public float m_Horizontal, m_Vertical, m_MoveSpeed;
    public bool m_WallLeft, m_WallRight, m_WallUp, m_WallDown;
    public Animator Ani;
	void Start () {
        Ani = transform.GetChild(0).GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.UpArrow) && !m_WallUp && !ScriptGameManager.isPlayerPause)
        {
            m_Horizontal = 0f;
            m_Vertical = 1f;
            Ani.speed = 1f;
            Ani.transform.localRotation = Quaternion.Euler(0, 0, 0f);
            //Ani.SetTrigger("Up");
        }
        else if (Input.GetKey(KeyCode.DownArrow) && !m_WallDown && !ScriptGameManager.isPlayerPause)
        {
            m_Horizontal = 0f;
            m_Vertical = -1f;
            Ani.speed = 1f;
            Ani.transform.localRotation = Quaternion.Euler(0, 0, 180f);
            //Ani.SetTrigger("Down");
        }
        else if (Input.GetKey(KeyCode.LeftArrow) && !m_WallLeft && !ScriptGameManager.isPlayerPause)
        {
            m_Horizontal = -1f;
            m_Vertical = 0f;
            Ani.speed = 1f;
            Ani.transform.localRotation = Quaternion.Euler(0, 0, 90f);
            //Ani.SetTrigger("Left");
        }
        else if (Input.GetKey(KeyCode.RightArrow) && !m_WallRight && !ScriptGameManager.isPlayerPause)
        {
            m_Horizontal = 1f;
            m_Vertical = 0f;
            Ani.speed = 1f;
            Ani.transform.localRotation = Quaternion.Euler(0, 0, 270f);
            //Ani.SetTrigger("Right");
        }

        if (m_Vertical == 1f && m_WallUp && !ScriptGameManager.isPlayerPause && !ScriptGameManager.isPlayerDead)
        {
            //m_Vertical = 1f;
            Ani.speed = 0f;
        }
        else if (m_Vertical == -1f && m_WallDown && !ScriptGameManager.isPlayerPause && !ScriptGameManager.isPlayerDead)
        {
            //m_Vertical = -1f;
            Ani.speed = 0f;
        }
        else if (m_Horizontal == -1f && m_WallLeft && !ScriptGameManager.isPlayerPause && !ScriptGameManager.isPlayerDead)
        {
            //m_Horizontal = 1f;
            Ani.speed = 0f;
        }
        else if (m_Horizontal == 1f && m_WallRight && !ScriptGameManager.isPlayerPause && !ScriptGameManager.isPlayerDead)
        {
            //m_Horizontal = -1f;
            Ani.speed = 0f;
        }
	}

    void FixedUpdate()
    {
        if (ScriptGameManager.isPlayerDead || ScriptGameManager.isPlayerWin || ScriptGameManager.isPlayerPause)
            return;
        transform.Translate(new Vector2((m_Horizontal * (m_MoveSpeed + ((float)ScriptGameManager.RoundCount_CalSpeed * 0.1f))) * Time.deltaTime, (m_Vertical * (m_MoveSpeed + ((float)ScriptGameManager.RoundCount_CalSpeed * 0.1f))) * Time.deltaTime));
    }
}
