﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptSpawnOfEnemy : MonoBehaviour {

    public Transform BaseOfEnemyPoint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            //Debug.Log(col.GetComponent<ScriptEnemyController>().SaveZoneTime + "||" + Time.time);
            if (col.GetComponent<ScriptEnemyController>().SaveZoneTime > Time.time)
            {
                if (transform.position.y > col.GetComponent<ScriptEnemyController>().transform.position.y)
                {
                    col.GetComponent<ScriptEnemyController>().WalkDirection = "Up";

                    col.GetComponent<ScriptEnemyController>().m_Vertical = 1;
                    col.GetComponent<ScriptEnemyController>().m_Horizontal = 0f;
                }
                else
                {
                    col.GetComponent<ScriptEnemyController>().WalkDirection = "Down";

                    col.GetComponent<ScriptEnemyController>().m_Vertical = -1;
                    col.GetComponent<ScriptEnemyController>().m_Horizontal = 0f;
                }

                col.GetComponent<ScriptEnemyController>().m_MoveSpeed = 0.6f;
            }
            else if (!col.GetComponent<ScriptEnemyController>().isNotIdle)
            {
                col.GetComponent<ScriptEnemyController>().isNotIdle = true;
            }
            else if (col.GetComponent<ScriptEnemyController>().isDead)
            {
                col.GetComponent<ScriptEnemyController>().isDead = false;
                col.GetComponent<ScriptEnemyController>().isBase = false;
                col.GetComponent<ScriptEnemyController>().isRespawn = true;
                //col.GetComponent<ScriptEnemyController>().isChaos = false;

                //col.GetComponent<ScriptEnemyController>().isTurn = true;
                col.GetComponent<ScriptEnemyController>().AIPoint = BaseOfEnemyPoint.position;
                col.GetComponent<ScriptEnemyController>().WalkDirection = "Up";

                col.GetComponent<ScriptEnemyController>().m_Vertical = 1;
                col.GetComponent<ScriptEnemyController>().m_Horizontal = 0f;

                col.GetComponent<ScriptEnemyController>().m_MoveSpeed = 0.5f;
                col.GetComponent<ScriptEnemyController>().Ani.SetTrigger("Up");
                //Debug.Log("On Respawn");
            }
        }
    }
}
