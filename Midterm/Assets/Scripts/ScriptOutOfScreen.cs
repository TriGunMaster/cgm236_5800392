﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptOutOfScreen : MonoBehaviour {

    public Transform RespawnOfEnemyPoint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            col.GetComponent<ScriptEnemyController>().isDead = false;
            col.GetComponent<ScriptEnemyController>().isBase = false;
            col.GetComponent<ScriptEnemyController>().isRespawn = true;

            //col.GetComponent<ScriptEnemyController>().isTurn = true;
            col.GetComponent<ScriptEnemyController>().AIPoint = RespawnOfEnemyPoint.position;
            col.GetComponent<ScriptEnemyController>().WalkDirection = "Up";

            col.GetComponent<ScriptEnemyController>().m_Vertical = 1;
            col.GetComponent<ScriptEnemyController>().m_Horizontal = 0f;

            col.GetComponent<ScriptEnemyController>().m_MoveSpeed = 0.3f;
            col.transform.position = RespawnOfEnemyPoint.position;

            Debug.Log("Destroy By Scene:" + col.transform.name);
        }
    }
}
