﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptEnemyController : MonoBehaviour {

    public bool m_WallUp, m_WallDown, m_WallLeft, m_WallRight;
    public float m_WallUp_Percent, m_WallDown_Percent, m_WallLeft_Percent, m_WallRight_Percent;

    public string WalkDirection, LastWalkDirection;
    public float m_Horizontal, m_Vertical, m_MoveSpeed;
    public bool isTurn, isDead, isBase, isRespawn, isDeadSet, isDebug, isLockPlayer, isChaos, isStartChaos, isPlayerEat;
    public Vector2 AIPoint;
    public Transform BaseOfEnemy, RespawnOfEnemy;
    public float RadiusDetect;
    public float SaveZoneTime;
    public float SaveZoneTime_Default;
    public bool isNotIdle; // ทำค้างไว้ต้องทำต่อให้มันเล่นอนิเมชั่นก่อนมันเกิดออกไปนอกฐาน
    public Vector2 m_StartPos, m_Vertical_Default;
    public string EnemyColor;

    public Animator Ani;
    string LastAni;
    void Awake()
    {
        SaveZoneTime = Time.time + SaveZoneTime_Default;
    }
	void Start () {
        m_Vertical_Default = new Vector2(m_Horizontal, m_Vertical);
        m_StartPos = transform.position;
        Ani = transform.GetChild(0).GetComponent<Animator>();
        //RadiusDetect = 0.05f;
	}

    void Moving()
    {
        if (isDead && m_MoveSpeed != 4 && isNotIdle)
        {
            m_MoveSpeed = 4;
            RadiusDetect = 0.5f + (ScriptGameManager.RoundCount_CalSpeed * 0.01f);

        }
        else if (!isDead && m_MoveSpeed != 2 && !isRespawn && !isChaos && isNotIdle)
        {
            m_MoveSpeed = 2f;
            RadiusDetect = 0.05f + (ScriptGameManager.RoundCount_CalSpeed * 0.01f);
        }
        else if (!isDead && m_MoveSpeed != 1.3 && !isRespawn && isChaos && isNotIdle)
        {
            m_MoveSpeed = 1.3f;
            RadiusDetect = 0.05f + (ScriptGameManager.RoundCount_CalSpeed * 0.01f);
        }
        //col.GetComponent<ScriptEnemyController>().SaveZoneTime > Time.time
        if (!isNotIdle && SaveZoneTime < Time.time && !ScriptGameManager.isPlayerDead && !ScriptGameManager.isPlayerWin && !ScriptGameManager.isPlayerPause && EnemyColor != "Red")
        {
            //Debug.Log(transform.name);
            if (transform.position.x <= RespawnOfEnemy.position.x) // Stay Left
            {
                transform.Translate(new Vector3((m_Horizontal * 0.3f) * Time.deltaTime, 0, 0));
                if (Mathf.Abs(transform.position.y) - Mathf.Abs(RespawnOfEnemy.position.y) < 0.1f)
                {
                    WalkDirection = "Right";
                    m_Vertical = 0;
                    m_Horizontal = 1f;
                    //isNotIdle = true;
                    isRespawn = true;
                    //Debug.Log("ssadasdasd");

                    if (Mathf.Abs(transform.position.x) - Mathf.Abs(RespawnOfEnemy.position.x) < 0.03f)
                    {
                        WalkDirection = "Up";
                        m_Vertical = 1f;
                        m_Horizontal = 0f;
                        isNotIdle = true;
                    }
                }
                //Debug.Log("aswawaaww");
            }
            else if (transform.position.x >= RespawnOfEnemy.position.x) // Stay Left
            {
                transform.Translate(new Vector3((m_Horizontal * -0.3f) * Time.deltaTime, 0, 0));
                if (Mathf.Abs(transform.position.y) - Mathf.Abs(RespawnOfEnemy.position.y) < 0.1f)
                {
                    WalkDirection = "Right";
                    m_Vertical = 0;
                    m_Horizontal = -1f;
                    //isNotIdle = true;
                    isRespawn = true;

                    if (Mathf.Abs(transform.position.x) - Mathf.Abs(RespawnOfEnemy.position.x) < 0.03f)
                    {
                        WalkDirection = "Up";
                        m_Vertical = 1f;
                        m_Horizontal = 0f;
                        isNotIdle = true;
                        //isRespawn = false;
                    }
                }
                //Debug.Log("aswawaaww");
            }
            //Debug.Log("zzasdasdas");
        }

        if (ScriptGameManager.isPlayerDead || ScriptGameManager.isPlayerWin || ScriptGameManager.isPlayerPause)
            return;
        transform.Translate(new Vector3((m_Horizontal * (m_MoveSpeed + ((float)ScriptGameManager.RoundCount_CalSpeed * 0.2f))) * Time.deltaTime, (m_Vertical * (m_MoveSpeed + ((float)ScriptGameManager.RoundCount_CalSpeed * 0.2f))) * Time.deltaTime, 0));
    }

    void TurnCorner()
    {
        if (Vector2.Distance(transform.position, AIPoint) < RadiusDetect)
        {
            transform.position = AIPoint;
            if (LastWalkDirection == WalkDirection)
            {
                //Debug.Log("Last:" + LastWalkDirection);
                float RandomWay = Random.Range(0, 100);
                float WayAvailable = 0;
                float Percent_Stack = 0;
                if (!m_WallUp && LastWalkDirection != "Up")
                {
                    WayAvailable++;
                    //Debug.Log("Add UP");
                }
                if (!m_WallDown && LastWalkDirection != "Down")
                {
                    WayAvailable++;
                    //Debug.Log("Add DOWN");
                }
                if (!m_WallLeft && LastWalkDirection != "Left")
                {
                    WayAvailable++;
                    //Debug.Log("Add LEFT");
                }
                if (!m_WallRight && LastWalkDirection != "Right")
                {
                    WayAvailable++;
                    //Debug.Log("Add RIGHT");
                }
                //Debug.Log("LastWayAvai:" + WayAvailable + "|Name:" + transform.name + "DirOld:" + WalkDirection);

                if (!m_WallUp && LastWalkDirection != "Up")
                {
                    Percent_Stack += 100 / WayAvailable;
                    m_WallUp_Percent = Percent_Stack;
                    //Debug.Log("Percent UP");
                }
                if (!m_WallDown && LastWalkDirection != "Down")
                {
                    Percent_Stack += 100 / WayAvailable;
                    m_WallDown_Percent = Percent_Stack;
                    //Debug.Log("Percent DOWN");
                }
                if (!m_WallLeft && LastWalkDirection != "Left")
                {
                    Percent_Stack += 100 / WayAvailable;
                    m_WallLeft_Percent = Percent_Stack;
                    //Debug.Log("Percent LEFT");
                }
                if (!m_WallRight && LastWalkDirection != "Right")
                {
                    Percent_Stack += 100 / WayAvailable;
                    m_WallRight_Percent = Percent_Stack;
                    //Debug.Log("Percent RIGHT");
                }

                if (RandomWay < m_WallUp_Percent && !m_WallUp && LastWalkDirection != "Up")
                    WalkDirection = "Up";
                else if (RandomWay < m_WallDown_Percent && !m_WallDown && LastWalkDirection != "Down")
                    WalkDirection = "Down";
                else if (RandomWay < m_WallLeft_Percent && !m_WallLeft && LastWalkDirection != "Left")
                    WalkDirection = "Left";
                else if (RandomWay < m_WallRight_Percent && !m_WallRight && LastWalkDirection != "Right")
                    WalkDirection = "Right";

                //Debug.Log("LastCalcu:" + WalkDirection);
            }

            if (LastAni == "" || LastAni == null) // Fix Bug no north
            {
                if (!m_WallUp)
                    WalkDirection = "Up";
                else if (!m_WallDown)
                    WalkDirection = "Down";
                else if (!m_WallLeft)
                    WalkDirection = "Left";
                else if (!m_WallRight)
                    WalkDirection = "Right";
                //Debug.Log(WalkDirection);
            }

            if (LastAni != WalkDirection)
            {
                LastAni = WalkDirection;
                if (!isChaos && !isDead)
                {
                    Ani.SetTrigger(LastAni);
                    //Debug.Log(transform.name + "|" + LastAni);
                }
                else if (isDead)
                {
                    Ani.SetTrigger("C" + LastAni);
                    //Debug.Log(transform.name + "|" + LastAni);
                }
            }

            //WalkDirection = "Right";
            if (WalkDirection == "Up")
            {
                m_Vertical = 1f;
                m_Horizontal = 0;
            }
            else if (WalkDirection == "Down")
            {
                m_Vertical = -1f;
                m_Horizontal = 0;
            }
            else if (WalkDirection == "Left")
            {
                m_Vertical = 0;
                m_Horizontal = -1f;
            }
            else if (WalkDirection == "Right")
            {
                m_Vertical = 0;
                m_Horizontal = 1f;
            }

            isTurn = false;
        }
        //Debug.Log("TurnCorner");
    }

    void DeadSet()
    {
        transform.position = AIPoint;
            float RandomWay = Random.Range(0, 100);
            float WayAvailable = 0;
            float Percent_Stack = 0;
            if (!m_WallUp && LastWalkDirection != "Up")
            {
                WayAvailable++;
            }
            if (!m_WallDown && LastWalkDirection != "Down")
            {
                WayAvailable++;
            }
            if (!m_WallLeft && LastWalkDirection != "Left")
            {
                WayAvailable++;
            }
            if (!m_WallRight && LastWalkDirection != "Right")
            {
                WayAvailable++;
            }
            //Debug.Log("LastWayAvai:" + WayAvailable + "|Name:" + transform.name + "DirOld:" + WalkDirection);

            if (!m_WallUp && LastWalkDirection != "Up")
            {
                Percent_Stack += 100 / WayAvailable;
                m_WallUp_Percent = Percent_Stack;
            }
            if (!m_WallDown && LastWalkDirection != "Down")
            {
                Percent_Stack += 100 / WayAvailable;
                m_WallDown_Percent = Percent_Stack;
            }
            if (!m_WallLeft && LastWalkDirection != "Left")
            {
                Percent_Stack += 100 / WayAvailable;
                m_WallLeft_Percent = Percent_Stack;
            }
            if (!m_WallRight && LastWalkDirection != "Right")
            {
                Percent_Stack += 100 / WayAvailable;
                m_WallRight_Percent = Percent_Stack;
            }

            if (RandomWay < m_WallUp_Percent && !m_WallUp && LastWalkDirection != "Up")
                WalkDirection = "Up";
            else if (RandomWay < m_WallDown_Percent && !m_WallDown && LastWalkDirection != "Down")
                WalkDirection = "Down";
            else if (RandomWay < m_WallLeft_Percent && !m_WallLeft && LastWalkDirection != "Left")
                WalkDirection = "Left";
            else if (RandomWay < m_WallRight_Percent && !m_WallRight && LastWalkDirection != "Right")
                WalkDirection = "Right";

        //WalkDirection = "Right";
        if (WalkDirection == "Up")
        {
            m_Vertical = 1f;
            m_Horizontal = 0;
        }
        else if (WalkDirection == "Down")
        {
            m_Vertical = -1f;
            m_Horizontal = 0;
        }
        else if (WalkDirection == "Left")
        {
            m_Vertical = 0;
            m_Horizontal = -1f;
        }
        else if (WalkDirection == "Right")
        {
            m_Vertical = 0;
            m_Horizontal = 1f;
        }

        isDeadSet = false;
        //Debug.Log("ASDASSAd");
    }

    IEnumerator StatusChaos()
    {
        Ani.SetTrigger("Dark");
        yield return new WaitForSeconds(5f - (ScriptGameManager.RoundCount_CalSpeed * 0.2f));
        Ani.SetTrigger("DarkWhite");
        yield return new WaitForSeconds(5f - (ScriptGameManager.RoundCount_CalSpeed * 0.2f));
        isChaos = false;
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (isTurn)
            TurnCorner();
        if (isDeadSet)
        {
            DeadSet();
        }

        if (isPlayerEat)
        {
            isDead = true;
            isDeadSet = true;
            isPlayerEat = false;
            StopAllCoroutines();
            Ani.SetTrigger("C" + LastAni);
            //isChaos = false;
            //isDebug = false;
        }

        if (isStartChaos)
        {
            isChaos = true;
            isStartChaos = false;
            StopAllCoroutines();
            StartCoroutine(StatusChaos());
        }

        if (ScriptGameManager.isPlayerDead)
        {
            if (EnemyColor != "Red")
            {
                m_Vertical = 0;
                m_Horizontal = 0f;
            }
            Ani.SetTrigger("PlayerDead");
        }
        if (ScriptGameManager.isPlayerRespawn)
        {
            if (EnemyColor != "Red")
            {
                isNotIdle = false;
                isRespawn = true;
                isDead = true;
            }
            m_Vertical = m_Vertical_Default.y;
            m_Horizontal = m_Vertical_Default.x;
            transform.position = m_StartPos;
        }

        // Random LockPlayer
        // Main Behaviour
        if (EnemyColor == "Red")
        {
            if (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) >= 15)
                isLockPlayer = true;
        }
        else if (EnemyColor == "Pink")
        {
            //float PercentOfLock = (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) * 100f) / 40f;
            //if (PercentOfLock >= 100f)
            //    PercentOfLock = 95;
            //if (PercentOfLock < Random.Range(0, 100))
            //{
            //    isLockPlayer = true;
            //}
            //else
            //{
            //    isLockPlayer = false;
            //}

            if (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) >= 30)
                isLockPlayer = true;

            // Main Behaviour
            //if (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) >= 30)
            //{
            //    if (isLockPlayer)
            //    {
            //        if (Time.time % 30 <= 1)
            //            isLockPlayer = false;
            //    }
            //    else
            //    {
            //        if (Time.time % 32 <= 1)
            //            isLockPlayer = true;
            //    }
            //}
        }
        else if (EnemyColor == "Blue")
        {
            //float PercentOfLock = (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) * 100f) / 50f;
            //if (PercentOfLock >= 100f)
            //    PercentOfLock = 90;
            //if (PercentOfLock < Random.Range(0, 100))
            //{
            //    isLockPlayer = true;
            //}
            //else
            //{
            //    isLockPlayer = false;
            //}

            if (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) >= 40)
                isLockPlayer = true;

            // Main Behaviour
            //if (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) >= 40)
            //{
            //    if (isLockPlayer)
            //    {
            //        if (Time.time % 20 <= 1)
            //            isLockPlayer = false;
            //    }
            //    else
            //    {
            //        if (Time.time % 25 <= 1)
            //            isLockPlayer = true;
            //    }
            //}
        }
        else if (EnemyColor == "Orange")
        {
            //float PercentOfLock = (Mathf.Abs(ScriptGameData.PlayerAliveTime - Time.time) * 100f) / 60f;
            //if (PercentOfLock >= 100f)
            //    PercentOfLock = 70;
            //if (PercentOfLock < Random.Range(0, 100))
            //{
            //    isLockPlayer = true;
            //}
            //else
            //{
            //    isLockPlayer = false;
            //}

            // Main Behaviour
            if (isLockPlayer)
            {
                if (Time.time % 20 <= 1)
                    isLockPlayer = false;
            }
            else
            {
                if (Time.time % 22 <= 1)
                    isLockPlayer = true;
            }
        }

        //Debug.Log(Time.time);

        Moving();
	}
}
