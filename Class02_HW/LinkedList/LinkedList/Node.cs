﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Node
    {
        public Object Element;
        public Node Link;

        public Node()
        {
            Element = null;
            Link = null;
        }

        public Node(Object obj)
        {
            Element = obj;
            Link = null;
        }
    }
}
