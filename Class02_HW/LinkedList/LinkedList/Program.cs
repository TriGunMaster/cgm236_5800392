﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            CLinkedList myList = new CLinkedList();
            ListIterator iter = new ListIterator(myList);

            string choice, value;

            iter.InsertAfter("Sataburo");
            iter.InsertAfter("WillMake");
            iter.InsertAfter("Smart");

            while (true)
            {
                Console.WriteLine("##########################");
                Console.WriteLine("(n) move to next node");
                Console.WriteLine("(g) get value in current node");
                Console.WriteLine("(f) reset iterator");
                Console.WriteLine("(s) show complete list");
                Console.WriteLine("(a) Insert After");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] onechar = choice.ToCharArray();

                switch (onechar[0])
                {
                    case 'n':
                        if ((!myList.IsEmpty()) && (!(iter.AtEnd())))
                        {
                            iter.NextLink();
                        }
                        else
                        {
                            Console.WriteLine("Can't move to the next node");
                        }
                        break;

                    case 'g':
                        if (!myList.IsEmpty())
                        {
                            Console.WriteLine("Element: " + iter.GetCurrent().Element);
                        }
                        else
                            Console.WriteLine("List is Empty");
                        break;

                    case 'f':
                        iter.Reset();
                        break;
                    case 'r':
                        iter.Remove();
                        break;

                    case 's':
                        if (!(myList.IsEmpty()))
                            myList.ShowList();
                        else
                            Console.WriteLine("List is Empty");
                        break;

                    case 'a':
                        Console.WriteLine();
                        Console.Write("Enter value to insert:");
                        value = Console.ReadLine();
                        iter.InsertAfter(value);
                        break;

                    default:
                        if (!(myList.IsEmpty()))
                            myList.ShowList();
                        else
                            Console.WriteLine("List is Empty");
                        break;

                }
            }

        }
    }
}
