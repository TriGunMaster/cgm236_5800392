﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class Program
    {
        static void Main(string[] args)
        {
            BST nums = new BST();
            nums.Insert(23);
            nums.Insert(45);
            nums.Insert(16);
            nums.Insert(37);
            nums.Insert(3);
            nums.Insert(99);
            nums.Insert(22);

            Console.WriteLine("BST : InOrder");
            nums.InOrder(nums.root);

            Console.WriteLine("BST : PreOrder");
            nums.PreOrder(nums.root);

            Console.WriteLine("BST : PostOrder");
            nums.PostOrder(nums.root);

            Console.WriteLine("MIN IS :");
            Console.WriteLine(nums.FindMin());

            Console.WriteLine("MAX IS :");
            Console.WriteLine(nums.FindMax());

            Console.Read();
        }
    }
}
