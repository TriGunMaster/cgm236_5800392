﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class BST
    {
        public Node root;

        public BST()
        {
            root = null;
        }

        public void Insert(int i)
        {
            Node newNode = new Node();
            newNode.Data = i;

            if (root == null)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;

                while (true)
                {
                    parent = current;
                    if (i < current.Data)
                    {
                        current = current.Left;
                        if (current == null)
                        {
                            parent.Left = newNode;
                            break;
                        }
                    }
                    else
                    {
                        current = current.Right;
                        if (current == null)
                        {
                            parent.Right = newNode;
                            break;
                        }
                    }
                }
            }
        }

        public void InOrder(Node rootnode)
        {
            if (!(rootnode == null))
            {
                InOrder(rootnode.Left);
                rootnode.DisplayNode();
                InOrder(rootnode.Right);
            }
        }

        public void PreOrder(Node rootnode)
        {
            if (!(rootnode == null))
            {
                rootnode.DisplayNode();
                PreOrder(rootnode.Left);
                PreOrder(rootnode.Right);
            }
        }

        public void PostOrder(Node rootnode)
        {
            if (!(rootnode == null))
            {
                PostOrder(rootnode.Left);
                PostOrder(rootnode.Right);
                rootnode.DisplayNode();
            }
        }

        public int FindMin()
        {
            Node current = root;
            while(!(current.Left == null))
            {
                current = current.Left;
            }
            return current.Data;
        }

        public int FindMax()
        {
            Node current = root;
            while(!(current.Right == null))
            {
                current = current.Right;
            }
            return current.Data;
        }
    }
}
