﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ProjectStack
{
    class CStack
    {
        int p_index;
        ArrayList list;

        public CStack()
        {
            list = new ArrayList();
            p_index = -1;
        }

        public int Count
        {
            get { return list.Count; }
        }

        public void Push(Object obj)
        {
            list.Add(obj);
            p_index++;
        }

        public Object Pop()
        {
            Object obj = list[p_index];
            list.RemoveAt(p_index);
            p_index--;
            return obj;
        }

        public void Clear()
        {
            list.Clear();
        }

        public Object Top()
        {
            return list[p_index];
        }
    }
}
