﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;

namespace ProjectQueue
{
    class CQueue
    {
        ArrayList arrayList;

        public CQueue()
        {
            arrayList = new ArrayList();
        }

        public void Enqueue(Object obj)
        {
            arrayList.Add(obj);
        }

        public void Dequeue()
        {
            arrayList.RemoveAt(0);
        }

        public Object Front()
        {
            return arrayList[0];
        }

        public int Count
        {
            get { return arrayList.Count; }
        }

        public void ClearQueue()
        {
            arrayList.Clear();
        }
    }
}
